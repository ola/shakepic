package compsci290.edu.duke.shakeapic;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ola on 2/23/17.
 */

public class RecyclerViewActivity extends Activity {

    private List<Gelato> mGelatoList;
    private RecyclerView mView;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private ShakeDetector mShakeDetector;


    private String sDATAFILE = "gelato.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.recyclerview_activity);

        mView =(RecyclerView)findViewById(R.id.rv);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        mView.setLayoutManager(llm);
        mView.setHasFixedSize(true);

        initializeData();
        initializeAdapter();

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mShakeDetector = new ShakeDetector();
        mShakeDetector.setOnShakeListener(new ShakeDetector.OnShakeListener() {

            @Override
            public void onShake(int count) {
                handleShakeEvent(count);
            }
        });

        Log.d("MAIN","create called");
    }

    @Override
    public void onResume(){
        super.onResume();
        // Add the following line to register the Session Manager Listener onResume
        mSensorManager.registerListener(mShakeDetector, mAccelerometer,	SensorManager.SENSOR_DELAY_UI);
        Log.d("MAIN","resume called");
    }
    @Override
    public void onPause() {
        // Add the following line to unregister the Sensor Manager onPause
        mSensorManager.unregisterListener(mShakeDetector);
        super.onPause();
        Log.d("MAIN","pause called");
    }

    private void handleShakeEvent(int count) {
        Log.d("SHAKELISTEN", "called");
        Toast.makeText(this,
                "shake it up",
                Toast.LENGTH_LONG).show();

        Collections.shuffle(mGelatoList);
        ((RVAdapter) mView.getAdapter()).updateList(mGelatoList);

    }

    private void initializeData() {
        loadAssets();
        mGelatoList = new ArrayList<>();
        mGelatoList.add(new Gelato("Hazelnut Chocolate Chip", "nutty+chocolatey=goodness", R.drawable.gelato1));
        mGelatoList.add(new Gelato("Key Lime Pie", "hints of the Florida Keys in a goodness that's without equal!", R.drawable.gelato2));
        mGelatoList.add(new Gelato("Caramel Apple Pie", "not for kids with braces", R.drawable.gelato3));
        mGelatoList.add(new Gelato("Alphonso Mango", "the taste is wonderful, the color is ...", R.drawable.gelato4));
        mGelatoList.add(new Gelato("Raspberry Chocolate", "swirls, twirls, whirls of raspberry goodness", R.drawable.gelato5));
        mGelatoList.add(new Gelato("Fudge Brownie", "so dark you can't see how good it is", R.drawable.gelato6));
        Log.d("MAIN", "gelato objects created");
    }
    private void loadAssets()  {


        Resources res = this.getResources();
        boolean firstWrite = false;


        try {
            FileInputStream inf = this.openFileInput(sDATAFILE);
        }
        catch (FileNotFoundException e1) {
            firstWrite = true;
            Log.d("READ", "reading first time");
            InputStream stream = res.openRawResource(R.raw.gelato_init);
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

            FileOutputStream fout = null;
            try {
                fout = this.openFileOutput(sDATAFILE, Context.MODE_PRIVATE);
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(fout));
                String line = null;
                while ((line = reader.readLine()) != null) {
                    String[] data = line.trim().split(",");
                    Log.d("READ", data[0]);
                    writer.write(line);
                    writer.newLine();
                }
                writer.close();
            } catch (IOException e) {
                Log.d("READ", "fail to read");
            }
        }
        if (!firstWrite) {
            try {
                FileInputStream inf = this.openFileInput(sDATAFILE);
                Log.d("READ", "have read previously");
                BufferedReader reader = new BufferedReader(new InputStreamReader(inf));
                String line = null;
                while ((line = reader.readLine()) != null) {
                    String[] data = line.trim().split(",");
                    Log.d("READ", data[0]);
                }
            } catch (IOException e) {
                e.printStackTrace();
                Log.d("BUG", "loading assets file");
            }
        }

    }

    private void initializeAdapter(){
        RVAdapter adapter = new RVAdapter(mGelatoList);
        mView.setAdapter(adapter);
    }
}