package compsci290.edu.duke.shakeapic;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by ola on 2/23/17.
 */

public class CardViewActivity extends Activity {

    TextView gelatoName;
    TextView gelatoComment;
    ImageView gelatoPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.cardview_activity);
        gelatoName = (TextView)findViewById(R.id.gelato_name);
        gelatoComment = (TextView)findViewById(R.id.gelato_comment);
        gelatoPhoto = (ImageView)findViewById(R.id.gelato_photo);

        gelatoName.setText("no name");
        gelatoComment.setText("no flavor");
        gelatoPhoto.setImageResource(R.drawable.gelato1);
    }
}