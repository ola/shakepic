package compsci290.edu.duke.shakeapic;

/**
 * Created by ola on 2/23/17.
 */

public class Gelato {
    String mName;
    String mComment;
    int mPhotoId;


    public Gelato(String name, String comment, int id){
        mName = name;
        mComment = comment;
        mPhotoId = id;
    }
}
