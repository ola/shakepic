package compsci290.edu.duke.shakeapic;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by ola on 2/23/17.
 */

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.GelatoViewHolder> {

    public static class GelatoViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView mGelatoName;
        TextView mGelatoComment;
        ImageView mGelatoPhoto;

        GelatoViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);
            mGelatoName = (TextView)itemView.findViewById(R.id.gelato_name);
            mGelatoComment = (TextView)itemView.findViewById(R.id.gelato_comment);
            mGelatoPhoto = (ImageView)itemView.findViewById(R.id.gelato_photo);

            itemView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    Toast.makeText(v.getContext(),
                            (String) mGelatoName.getText(),
                            Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    List<Gelato> mGelatoList;

    public RVAdapter(List<Gelato> list){
        this.mGelatoList = list;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public GelatoViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);
        GelatoViewHolder gvh = new GelatoViewHolder(v);
        return gvh;
    }

    @Override
    public void onBindViewHolder(GelatoViewHolder gelatoViewHolder, int i) {
        Log.d("RV","name id: "+gelatoViewHolder.itemView.findViewById(R.id.gelato_name));
        gelatoViewHolder.mGelatoName.setText(mGelatoList.get(i).mName);
        gelatoViewHolder.mGelatoComment.setText(mGelatoList.get(i).mComment);
        gelatoViewHolder.mGelatoPhoto.setImageResource(mGelatoList.get(i).mPhotoId);
    }

    @Override
    public int getItemCount() {
        return mGelatoList.size();
    }

    public void updateList(List<Gelato> list){
        mGelatoList = list;
        this.notifyDataSetChanged();
    }
}